package com.hausler.tim.handsoffcpr;

import android.content.Intent;
import android.util.Log;


import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

/**
 * Created by Ken Laptop on 11/28/2015.
 */
public class WatchListenerService extends WearableListenerService {

    private static final String GO_ACTIVITY = "/go_activity";
    private static final String STOP_ACTIVITY = "/stop_activity";

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {

        if (messageEvent.getPath().equalsIgnoreCase(GO_ACTIVITY)) {
            Intent intent = new Intent(this, HandPlacementActivity.class);
            //Flag needed for new activity from service
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Log.d("WatchListenerService", "starting practice session");
            startActivity(intent);
        } else if (messageEvent.getPath().equalsIgnoreCase(STOP_ACTIVITY)) {
            Intent intent = new Intent(this, EndActivity.class);
            Log.d("WatchListenerService", "ending practice session");
            //Flag needed for new activity from service
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            super.onMessageReceived(messageEvent);
        }

    }

}
