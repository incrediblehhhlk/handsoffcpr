package com.hausler.tim.handsoffcpr;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import org.apache.commons.math3.analysis.interpolation.SplineInterpolator;
import org.apache.commons.math3.analysis.polynomials.PolynomialSplineFunction;
import org.jtransforms.fft.DoubleFFT_1D;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tim on 11/3/2015.
 */
public class DataPoller {


    private static final String STATUS_UPDATE_CPR = "status_update_cpr";
    private static final String TAG = "Data Poller Listener";
    private SensorManager mSensorManager;
    private Sensor mSensor;
    private Context context;

    //store old data
    List<Long> times = new ArrayList<Long>();
    List<Double> mags = new ArrayList<Double>();


    public DataPoller(Context context) {
        Log.d("Data Poller", "Created Data Poller");
        mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        this.context = context;

    }


    public void start() {
        Log.d("Data Poller", "Started Data Poller");
        SensorEventListener listener = new DataPollerListener();
        mSensorManager.registerListener(listener, mSensor, SensorManager.SENSOR_DELAY_FASTEST);
    }

    public void stop() {
    }

    public class DataPollerListener implements SensorEventListener {

        @Override
        public void onSensorChanged(SensorEvent event) {
            Log.d(TAG, "heard event");
            long time = System.currentTimeMillis();
            double mag = Math.sqrt(
                    event.values[0] * event.values[0] +
                            event.values[1] * event.values[1] +
                            event.values[2] * event.values[2]);
            if (times.size() == 0) {
                mags.add(mag);//new stuff to the end
                times.add(time);//new stuff to the end
                return;
            }
            if (time != times.get(times.size() - 1)) {//don't add dups
                mags.add(mag);//new stuff to the end
                times.add(time);//new stuff to the end
            }
            int numSamples = mags.size();
            if (times.get(numSamples - 1) - times.get(0) >= 3000) {
                Log.d(TAG, "interpolating");
                //interpolate
                double[] doubleTimes = longToDoubleArrayMap(times);//change it to doubles
                double[] doubleMags = doubleToDoubleArrayMap(mags);
                PolynomialSplineFunction interpolator = new SplineInterpolator().interpolate(doubleTimes, doubleMags);
                double stepSize = (doubleTimes[numSamples - 1] - doubleTimes[0]) / numSamples;
                double[] resampledMags = resample(interpolator, stepSize, numSamples);

                //calculate fft
                DoubleFFT_1D fft = new DoubleFFT_1D(numSamples);
                fft.realForward(resampledMags);
                //format magnitudes into a string
                Log.d(TAG, formatfreqs(resampledMags, stepSize, numSamples));
                //find highest record(s)
                double maxFreq = findMaxFreq(resampledMags, stepSize, numSamples);
                Log.d(TAG, "max freq: " + maxFreq);
                //broadcast
                Intent intent = new Intent(STATUS_UPDATE_CPR);
                intent.putExtra("maxFreq", maxFreq);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                Log.d(TAG, "broacasted");
                cleanRecords(times, mags);
            }
        }

        private double findMaxFreq(double[] freqMags, double stepSize, int numSamples) {
            double resultFreq = 0d;
            double bestMag = 0d;
            for (int i = 1; i < freqMags.length; i++) {
                double freq = i / (numSamples * (stepSize / 1000));
                double mag = freqMags[i] * freqMags[i];
                if (mag > bestMag) {
                    bestMag = mag;
                    resultFreq = freq;
                }
           }
            return resultFreq;

        }

        private void cleanRecords(List<Long> times, List<Double> mags) {
            Long cutoffTime = times.get(times.size() - 1) - 1500; //all times before a second ago
            while (times.get(0) < cutoffTime) {
                times.remove(0);
                mags.remove(0);
            }
            assert times.size() == mags.size();
        }

        private String formatfreqs(double[] freqMags, double stepSize, int numSamples) {
            Log.d(TAG, "step size: " + stepSize + " num samples: " + numSamples);
            String result = "Frequencies:";
            for (int i = 0; i < freqMags.length; i++) {
                double freq = i / (numSamples * (stepSize / 1000));
                result = result + " " + freq + " Hz, mag:" + freqMags[i] + ";";
            }
            return result;
        }

        private double[] resample(PolynomialSplineFunction interpolator, double stepSize, int numSamples) {
            double earliestTime = interpolator.getKnots()[0];
            double[] result = new double[numSamples];
            for (int i = 0; i < numSamples; i++) {
                double samplePoint = i * stepSize + earliestTime;
                result[i] = interpolator.value(samplePoint);
            }
            return result;
        }

        private double[] doubleToDoubleArrayMap(List<Double> doubles) {
            double[] result = new double[doubles.size()];
            for (int i = 0; i < doubles.size(); i++) {
                result[i] = doubles.get(i).doubleValue();
            }
            return result;
        }

        private double[] longToDoubleArrayMap(List<Long> longs) {
            double[] result = new double[longs.size()];
            for (int i = 0; i < longs.size(); i++) {
                result[i] = longs.get(i).doubleValue();
            }
            return result;
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    }
}
