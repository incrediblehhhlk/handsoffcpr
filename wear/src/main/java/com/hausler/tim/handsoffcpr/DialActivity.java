package com.hausler.tim.handsoffcpr;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.RotateDrawable;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class DialActivity extends Activity {


    private static final String STATUS_UPDATE_CPR = "status_update_cpr";
    private static final String TAG = "Dial Activity";
    private int prevPosition = 0; //starts pointing left
    private static final int[] staticAnims = {R.anim.static0, R.anim.static15, R.anim.static30,
            R.anim.static45, R.anim.static60, R.anim.static75,
            R.anim.static90, R.anim.static105, R.anim.static120,
            R.anim.static135, R.anim.static150, R.anim.static165,
            R.anim.static180};
    private static final int[] moveAnims = {R.anim.move0to15, R.anim.move15to30, R.anim.move30to45,
            R.anim.move45to60, R.anim.move60to75, R.anim.move75to90,
            R.anim.move90to105, R.anim.move105to120, R.anim.move120to135,
            R.anim.move135to150, R.anim.move150to165, R.anim.move165to180};

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "received a broadcast " + intent.getAction());
            // TODO Auto-generated method stub
            // Get extra data included in the Intent
            if (STATUS_UPDATE_CPR.equals(intent.getAction())) {
                double maxFreq = intent.getDoubleExtra("maxFreq", -1d);
                if (maxFreq == -1d) {
                    Log.d(TAG, "couldn't get the maximum frequency from broadcast message");
                }
                setContentView(R.layout.activity_dial);


                //rotation animation
                int start = prevPosition;
                //calculate finish
                int finish = calculateNewPosition(maxFreq);
                ImageView dialView = (ImageView) findViewById(R.id.dialhand);

                animate(dialView, start, finish);
                //draw it in place
                prevPosition = finish;

                Log.d(TAG, "rotated");
            } else {
                Log.d(TAG, "received unknown action in broadcast");
            }
        }
    };

    private void animate(ImageView dialView, int start, int finish) {
        animateMovement(dialView, start, finish);
        //move the hand
        //stop the hand
        Animation a = AnimationUtils.loadAnimation(DialActivity.this, staticAnims[finish]);
//        a.setInterpolator(new PartialArcInterpolator(start, finish));
        a.setDuration(2500);
        a.setStartOffset(1400);
        dialView.startAnimation(a);
    }

    private void animateMovement(ImageView dialView, int start, int finish) {
        if (start == finish) return;
        int direction = calculateDirection(start, finish);
        int duration = 1400 / Math.abs(start - finish);
        int n = 0;
        if (direction > 0)
            for (int i = start; i < finish; i = i + 1) {
                Animation a = AnimationUtils.loadAnimation(DialActivity.this, moveAnims[i]);
                a.setDuration(duration);
                a.setStartOffset(duration * n);
                dialView.startAnimation(a);
//                try {
                Log.d(TAG, "moved forward from " + start + " to " + finish + ", offet: " + duration * n);
//                    Thread.sleep(duration);
//                } catch (InterruptedException e) {
//                    Log.d(TAG, "interrupted while sleeping");
//                }
                n += 1;
            }
        else
            for (int i = start - 1; i >= finish; i = i - 1) {
                Animation a = AnimationUtils.loadAnimation(DialActivity.this, moveAnims[i]);
                if (direction < 0)
                    a.setInterpolator(new ReverseInterpolator());
                a.setDuration(duration);
                a.setStartOffset(duration * n);
                dialView.startAnimation(a);
//                try {
                Log.d(TAG, "moved backward from " + start + " to " + finish + ", offet: " + duration * n);
//                    Thread.sleep(duration);
//                } catch (InterruptedException e) {
//                    Log.d(TAG, "interrupted while sleeping");
//                }
                n += 1;

            }
    }

    private int calculateDirection(int start, int finish) {
        if (start > finish) //moving left to right
            return -1;
        else  //moving right to left
            return 1;
    }

    private int calculateNewPosition(double maxFreq) {
        if (maxFreq < 1.0)//leftmost
            return 12;
        if (maxFreq < 2.0)
            return 11;
        if (maxFreq < 2.5)
            return 10;
        if (maxFreq < 3.0)
            return 9;
        if (maxFreq < 3.5)
            return 8;
        if (maxFreq < 4.0)
            return 6;
        if (maxFreq < 5.0)
            return 4;
        if (maxFreq < 6.0)
            return 3;
        if (maxFreq < 7.0)
            return 2;
        if (maxFreq < 8.0)
            return 1;
        if (maxFreq < 10)//rightmost
            return 0;
        return 0;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        startService(new Intent(this, AccelSignalService.class));
        startService(new Intent(this, WearLayerService.class));
        setContentView(R.layout.activity_dial);

        findViewById(R.id.invisiblebutton).setOnClickListener(new View.OnClickListener() {//TODO replace with actual field
            @Override
            public void onClick(View v) {
                Intent i = new Intent(DialActivity.this, EndActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);//TODO replace with real destination
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(
                mMessageReceiver, new IntentFilter(STATUS_UPDATE_CPR));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_dial, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

public class ReverseInterpolator implements Interpolator {
    @Override
    public float getInterpolation(float paramFloat) {
        return Math.abs(paramFloat - 1f);
    }
}
}
