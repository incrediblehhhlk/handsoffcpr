package com.hausler.tim.handsoffcpr;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class AccelSignalService extends Service {
    public AccelSignalService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //create an data poller
        new DataPoller(this).start();
        //start it
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //destroy data poller
    }
}
