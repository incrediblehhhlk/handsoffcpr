package com.hausler.tim.handsoffcpr;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.wearable.activity.WearableActivity;
import android.support.wearable.view.BoxInsetLayout;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class MainActivity extends WearableActivity {

    private static final SimpleDateFormat AMBIENT_DATE_FORMAT =
            new SimpleDateFormat("HH:mm", Locale.US);
    private static final String STATUS_UPDATE_CPR = "status_update_cpr";
    private static final String TAG = "Main Activity";
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "received a broadcast " + intent.getAction());
            // TODO Auto-generated method stub
            // Get extra data included in the Intent
            if (STATUS_UPDATE_CPR.equals(intent.getAction())) {
//                double maxFreq = intent.getDoubleExtra("maxFreq", -1d);
//                if (maxFreq == -1d) {
//                    Log.d(TAG, "couldn't get the maximum frequency from broadcast message");
//                }
//                setContentView(R.layout.activity_main);
//                TextView t = (TextView)findViewById(R.id.text);
//                t.setText(Double.toString(maxFreq));
//                t.invalidate();
            } else {
                Log.d("Main Activity", "received unknown action in broadcast");
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setAmbientEnabled();
        startService(new Intent(this, WatchListenerService.class));
        Log.d(TAG, "started watch listener service...");


        findViewById(R.id.invisiblebutton).setOnClickListener(new View.OnClickListener() {// TODO: replace with real field
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, HandPlacementActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);//TODO replace with real destination
            }
        });

    }

    @Override
    public void onEnterAmbient(Bundle ambientDetails) {
        super.onEnterAmbient(ambientDetails);
        updateDisplay();
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(
                mMessageReceiver, new IntentFilter(STATUS_UPDATE_CPR));
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(
                mMessageReceiver);
        Log.d(TAG, "Unregistered listener");
    }

    @Override
    public void onUpdateAmbient() {
        super.onUpdateAmbient();
        updateDisplay();
    }

    @Override
    public void onExitAmbient() {
        updateDisplay();
        super.onExitAmbient();
    }

    private void updateDisplay() {

    }
}
