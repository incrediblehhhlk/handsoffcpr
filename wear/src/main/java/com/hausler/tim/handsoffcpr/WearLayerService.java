package com.hausler.tim.handsoffcpr;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class WearLayerService extends Service {

    private static final String TAG = "Wear Layer Service";
    private static final String STATUS_UPDATE_CPR = "status_update_cpr";
    private GoogleApiClient mApiClient;
    private List<Long> times = new ArrayList<Long>();
    private List<Double> freqs = new ArrayList<Double>();
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("Main Activity", "received a broadcast " + intent.getAction());
            // TODO Auto-generated method stub
            // Get extra data included in the Intent
            if (STATUS_UPDATE_CPR.equals(intent.getAction())) {
                double maxFreq = intent.getDoubleExtra("maxFreq", -1d);
                if (maxFreq == -1d) {
                    Log.d(TAG, "couldn't get the maximum frequency from broadcast message");
                }
                //record what happened
                Long time = System.currentTimeMillis();
                times.add(time);
                freqs.add(maxFreq);
                //if we have enough...send message
                int numSamples = times.size();
                long latest = times.get(numSamples - 1);
                long oldest = times.get(0);
                if (latest - oldest >= 10000) {
                    String feedback = createFeedback(freqs);
                    sendMessage("/feedbackMessage", feedback);
                    cleanRecords(times, freqs);
                }
            } else {
                Log.d(TAG, "received unknown action in broadcast");
            }
        }
    };

    private String createFeedback(List<Double> freqs) {
        double sum = 0;
        for (int i = 0; i < freqs.size(); i++) {
            sum += freqs.get(i) / freqs.size();
        }
        try {
            JSONObject cprJson = new JSONObject().put("frequency", sum);
             cprJson.put("pressure", 0);//FIXME actually calculate the pressure
            return cprJson.toString();

        } catch (JSONException e) {
            Log.d(TAG, e.getMessage());
        }
        return null;
    }

    public WearLayerService() {
    }

    @Override
    public void onCreate() {
        initGoogleApiClient();
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter(STATUS_UPDATE_CPR));
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }


    private void sendMessage(final String path, final String text) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes(mApiClient).await();
                for (Node node : nodes.getNodes()) {
                    MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(
                            mApiClient, node.getId(), path, text.getBytes()).await();
                }
                Log.d("Wear Layer Service", "sent message path: " + path + " text: " + text);
            }
        }).start();
    }

    private void initGoogleApiClient() {
        mApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .build();

        mApiClient.connect();
    }

    private void cleanRecords(List<Long> times, List<Double> mags) {
        times.removeAll(times);
    }
}
