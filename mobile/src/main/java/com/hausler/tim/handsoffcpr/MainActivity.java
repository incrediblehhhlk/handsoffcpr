package com.hausler.tim.handsoffcpr;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00B799")));

        Button settings_button = (Button) findViewById(R.id.settings);
        final Intent settingsIntent = new Intent(this, SettingsActivity.class);
        settings_button.setVisibility(View.VISIBLE);
        settings_button.setTextColor(Color.TRANSPARENT);
        settings_button.setBackgroundColor(Color.TRANSPARENT);
        settings_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(settingsIntent);

            }
        });

        Button walkthrough_button = (Button) findViewById(R.id.walkthrough);
        final Intent walkthroughIntent = new Intent(this, WalkthroughActivity.class);
        walkthrough_button.setVisibility(View.VISIBLE);
        walkthrough_button.setTextColor(Color.TRANSPARENT);
        walkthrough_button.setBackgroundColor(Color.TRANSPARENT);
        walkthrough_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(walkthroughIntent);

            }
        });

        Button practice_button = (Button) findViewById(R.id.practice);
        final Intent practiceIntent = new Intent(this, PracticeActivity.class);
        practice_button.setVisibility(View.VISIBLE);
        practice_button.setTextColor(Color.TRANSPARENT);
        practice_button.setBackgroundColor(Color.TRANSPARENT);
        practice_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(practiceIntent);

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
