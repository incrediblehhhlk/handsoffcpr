package com.hausler.tim.handsoffcpr;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

/**
 * Created by Ken Laptop on 11/26/2015.
 */
public class WalkthroughActivity extends AppCompatActivity {

    private GoogleApiClient client;
    final private String GO_ACTIVITY = "/go_activity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Context c = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_walkthrough);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00B799")));

        //Initialize GoogleApiClient for message passing
        client = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle connectionHint) {
                        //Successfully Connected
                    }

                    @Override
                    public void onConnectionSuspended(int cause) {
                        //Connection was interrupted
                    }
                }).build();

        final ImageView walkthru1 = (ImageView) findViewById(R.id.walkthrough1);
        final ImageView walkthru2 = (ImageView) findViewById(R.id.walkthrough2);
        final ImageView walkthru3 = (ImageView) findViewById(R.id.walkthrough3);
        final ImageView walkthru4 = (ImageView) findViewById(R.id.walkthrough4);
        final ImageView walkthru5 = (ImageView) findViewById(R.id.walkthrough5);

        walkthru1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setVisibility(View.GONE);
            }
        });

        walkthru2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setVisibility(View.GONE);
            }
        });

        walkthru3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setVisibility(View.GONE);
            }
        });

        walkthru4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setVisibility(View.GONE);
            }
        });

        walkthru5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                client.connect();
                sendMessage(GO_ACTIVITY, "");
                startPractice();
            }
        });

        Button start_button = (Button) findViewById(R.id.start);
        final Intent startIntent = new Intent(this, PracticeActivity.class);
        start_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                client.connect();
                sendMessage(GO_ACTIVITY, "");
                startPractice();
            }
        });

    }

    private void startPractice() {
        Intent practiceIntent = new Intent(this, PracticeActivity.class);
        startActivity(practiceIntent);
    }

    //Send a message to the WatchListenerService
    private void sendMessage(final String path, final String text) {
        new Thread (new Runnable() {
            @Override
            public void run() {
                NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes(client).await();
                for(Node node : nodes.getNodes()) {
                    MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(
                            client, node.getId(), path, text.getBytes()).await();
                }
            }
        }).start();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        client.disconnect();
    }

}
