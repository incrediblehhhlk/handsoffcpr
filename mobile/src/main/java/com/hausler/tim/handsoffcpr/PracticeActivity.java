package com.hausler.tim.handsoffcpr;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

/**
 * Created by Ken Laptop on 11/26/2015.
 */
public class PracticeActivity extends AppCompatActivity {

    private GoogleApiClient client;
    final private String STOP_ACTIVITY = "/stop_activity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practice);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00B799")));

        //Initialize GoogleApiClient for message passing
        client = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle connectionHint) {
                        //Successfully Connected
                    }

                    @Override
                    public void onConnectionSuspended(int cause) {
                        //Connection was interrupted
                    }
                }).build();

        ImageView pauseButton = (ImageView) findViewById(R.id.pause);
        pauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                client.connect();
                sendMessage(STOP_ACTIVITY, "");
                displayFeedback();
            }
        });
    }

    private void displayFeedback() {
        Intent feedbackActivity = new Intent(this, PreviousActivity.class);
        startActivity(feedbackActivity);
    }

    //Send a message to the WatchListenerService
    private void sendMessage(final String path, final String text) {
        new Thread (new Runnable() {
            @Override
            public void run() {
                NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes(client).await();
                for(Node node : nodes.getNodes()) {
                    MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(
                            client, node.getId(), path, text.getBytes()).await();
                }
            }
        }).start();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        client.disconnect();
    }

}
